package com.example.workshopproduct.controller;

import com.example.workshopproduct.model.Product;
import com.example.workshopproduct.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ProductController {

    private ProductRepository productRepository;

    @GetMapping("/get-products")
    public List<Product> getIndex() {
        return productRepository.findAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/add-product")
    public void create(@RequestBody Product product) {
        productRepository.save(product);
    }
}
