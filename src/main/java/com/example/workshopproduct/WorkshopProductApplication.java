package com.example.workshopproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkshopProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkshopProductApplication.class, args);
    }

}
