# workshop-product

Jednoduchá GET/POST Spring aplikace

## Jak spustit tuto aplikaci

1. Vytvořte jar soubor pomocí Maven prostřednictvím menu v IntelliJ nebo pomocí příkazů 
`mvn clean` a
`mvn install`

2. Následující příkazy spouštějte přesně jeden po druhém:

```
docker network create my-network

docker build -f Dockerfile-database -t postgres-image .
docker run -p 5432:5432 --name postgres-container --network my-network postgres-image

docker build -f Dockerfile-app -t app-image .
docker run -p 8080:8080 --name app-container --network my-network app-image
```

Po spuštění kontejneru s databází bude terminál zaneprázdněn. Kontejner s aplikací musí být spuštěn v jiném 
terminálu nebo přidejte parametr `-d` do příkazu `run`

Pomocí příkazu `docker ps -a` můžete zkontrolovat, zda kontejnery běží.

Samotná aplikace je k dispozici na http://localhost:8080/ a má přesně 2 endpointy: GET `get-products` a POST `add-product`.
V IntelliJ v souboru `product.http` je můžete spustit

# Nejjednodušší způsob, jak spustit aplikaci

Všechny popsané akce pro spuštění aplikace jsou taky v bash skriptu `first_launch_script.sh`

```
sudo chmod +x first_launch_script.sh
./first_launch_script.sh
```
