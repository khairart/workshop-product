#!/bin/bash

mvn clean
mvn install

docker network create my-network

docker build -f Dockerfile-database -t postgres-image .
docker run -d -p 5432:5432 --name postgres-container --network my-network postgres-image

docker build -f Dockerfile-app -t app-image .
docker run -d -p 8080:8080 --name app-container --network my-network app-image
