CREATE TABLE Product (
    id INTEGER NOT NULL,
    name VARCHAR(255) NOT NULL,
    price NUMERIC(10, 2) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO product (id, name, price) VALUES
    (1, 'Product A', 10.99),
    (2, 'Product B', 24.99),
    (3, 'Product C', 15.49);